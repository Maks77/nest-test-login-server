import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDTO {
  @ApiProperty({type: String})
  login: string

  @ApiProperty({type: String})
  password: string
}