import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { UsersService } from '../service/users.service';
import { User } from '../entity/user.entity';
import { CreateUserDTO } from '../dto/create_user.dto';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { JWTGuard } from '../../auth/guards/jwt.guard';

@ApiTags('User')
@Controller('user')
export class UsersController {
  constructor(
    private readonly usersService: UsersService
  ) {}

  @Get('/')
  @UseGuards(JWTGuard)
  @ApiBearerAuth()
  async getAllUsers(): Promise<User[]> {
    return this.usersService.findAll()
  }

  @Get(':id')
  @UseGuards(JWTGuard)
  @ApiBearerAuth()
  getUserById(@Param('id') id: string): Promise<User | undefined> {
    return this.usersService.findById(id)
  }

  @Delete(':id')
  @UseGuards(JWTGuard)
  @ApiBearerAuth()
  deleteUser(@Param('id') id: string): Promise<void> {
    return this.usersService.remove(id)
  }

  @Post()
  @UseGuards(JWTGuard)
  @ApiBearerAuth()
  @ApiBody({ type: CreateUserDTO })
  createUser(@Body() user: CreateUserDTO): Promise<User> {
    console.log('createUser', user);
    return this.usersService.create({
      login: user.login,
      password: user.password
    })
  }

  @Put(':id')
  @UseGuards(JWTGuard)
  @ApiBearerAuth()
  updateUser(
    @Param('id') id: string,
    @Body() user: User
  ): Promise<User> {
    return this.usersService.update(user)
  }
}
