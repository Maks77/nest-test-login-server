import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../base.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity('users')
export class User extends BaseEntity {

  @ApiProperty({ type: String })
  @Column({ type: 'varchar', length: 100, nullable: false })
  login: string;

  @ApiProperty({ type: String })
  @Column({ type: 'varchar', length: 100, nullable: false })
  password?: string;
}