import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entity/user.entity';
import { compare } from 'bcrypt';
import { RegisterRequestDTO } from '../../auth/dto/register_req.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>) {}


  async createUserFromRequest(request: RegisterRequestDTO): Promise<User> {
    const {login, password} = request
    const existingByLogin = await this.findByLogin(login)
    console.log('existingByLogin', existingByLogin);
    if (existingByLogin) {
      throw new UnprocessableEntityException('Username already in use')
    }

    return this.create({ login, password })
  }

  async validateCredentials(user: User, password: string): Promise<boolean> {
    // todo: change when bcript will work
    // return compare(password, user.password)
    return password === user.password
  }



  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findById(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  findByLogin(log: string): Promise<User> {
    return this.usersRepository.findOne({where: {login: log}})
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  create(user: User): Promise<User> {
    return this.usersRepository.save<User>(user)
  }

  async update(user: User): Promise<User> {
    const existedUser = await this.usersRepository.findOneOrFail(user.id)
    existedUser.login = user.login
    existedUser.password = user.password
    return this.usersRepository.save<User>(existedUser)
  }
}