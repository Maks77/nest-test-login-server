
import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { UsersService } from '../../users/service/users.service';
import { User } from '../../users/entity/user.entity';
import { ConfigService } from '@nestjs/config';


export interface AccessTokenPayload {
  sub: string;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

  public constructor (
    private userService: UsersService,
    private configService: ConfigService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET'),
      signOptions: {
        expiresIn: configService.get<string>('JWT_EXPIRES_IN'),
      },
    })

    this.userService = userService
  }

  async validate (payload: AccessTokenPayload): Promise<User> {
    const { sub: id } = payload

    const user = await this.userService.findById(id)

    if (!user) {
      return null
    }

    return user
  }
}