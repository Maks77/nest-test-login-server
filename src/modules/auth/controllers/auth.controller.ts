import { User } from '../../users/entity/user.entity';
import {
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Req,
  UnauthorizedException,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { UsersService } from '../../users/service/users.service';
import { TokensService } from '../services/tokens.service';
import { RefreshRequestDTO} from '../dto/refresh_req.dto';
import { JWTGuard } from '../guards/jwt.guard';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { LoginRequestDTO } from '../dto/login_req.dto';
import { RegisterRequestDTO } from '../dto/register_req.dto';

export interface AuthenticationPayload {
  user: User
  payload: {
    type: string
    token: string
    refresh_token?: string
  }
}

const REFRESH_TOKEN_EXPIRATION_TIME = 60 * 60 * 24 * 30 // 60 * 60 * 24 * 30

@ApiTags("Auth")
@Controller('auth')
export class AuthController {

  constructor(
    private users: UsersService,
    private tokens: TokensService
  ) {}


  @ApiBody({ type: RegisterRequestDTO })
  @Post('register')
  public async register(@Body(ValidationPipe) body: RegisterRequestDTO) {
    const user = await this.users.createUserFromRequest(body)
    const token = await this.tokens.generateAccessToken(user)
    const refresh = await this.tokens.generateRefreshToken(user, REFRESH_TOKEN_EXPIRATION_TIME)
    const payload = this.buildResponsePayload(user, token, refresh)

    return {
      status: 'success',
      data: payload
    }
  }

  @ApiBody({ type: LoginRequestDTO })
  @Post('login')
  public async login(@Body(ValidationPipe) body: LoginRequestDTO) {
    const { login, password } = body
    const user = await this.users.findByLogin(login)
    const valid = user ? await this.users.validateCredentials(user, password) : false

    if (!valid) {
      throw new UnauthorizedException('The login is invalid')
    }

    const token = await this.tokens.generateAccessToken(user)
    const refresh = await this.tokens.generateRefreshToken(user, REFRESH_TOKEN_EXPIRATION_TIME)

    const payload = this.buildResponsePayload(user, token, refresh)

    return {
      status: 'success',
      data: payload
    }
  }

  @ApiBody({ type: RefreshRequestDTO })
  @Post('/refresh')
  public async refresh (@Body(ValidationPipe) body: RefreshRequestDTO) {
    const { user, token } = await this.tokens.createAccessTokenFromRefreshToken(body.refresh_token)
    const newRefreshToken = await this.tokens.generateRefreshToken(user, REFRESH_TOKEN_EXPIRATION_TIME)
    const payload = this.buildResponsePayload(user, token, newRefreshToken)

    return {
      status: 'success',
      data: payload,
    }
  }

  @Get('me')
  @UseGuards(JWTGuard)
  @ApiBearerAuth()
  public async getUser(@Req() request) {
    const userId = request.user.id
    const user = await this.users.findById(userId)
    return {
      status: 'success',
      data: user
    }
  }



  private buildResponsePayload (user: User, accessToken: string, refreshToken?: string): AuthenticationPayload {
    return {
      user: user,
      payload: {
        type: 'bearer',
        token: accessToken,
        ...(refreshToken ? { refresh_token: refreshToken } : {}),
      }
    }
  }


}