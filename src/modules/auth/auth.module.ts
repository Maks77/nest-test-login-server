import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { AuthGuard, PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { JWTGuard } from './guards/jwt.guard';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RefreshToken } from './entity/refresh-token.entity';
import { AuthController } from './controllers/auth.controller';
import { TokensService } from './services/tokens.service';
import { RefreshTokenRepository } from './services/refresh-token.repository';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forFeature([RefreshToken]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET'),
          signOptions: { expiresIn: configService.get<string>('JWT_EXPIRES_IN') }
        }
      },
      inject: [ConfigService]
    })
  ],
  controllers: [AuthController],
  providers: [
    TokensService,
    JwtStrategy,
    {
      provide: 'REFRESH_REPOSITORY',
      useClass: RefreshTokenRepository
    }
  ],
})
export class AuthModule {}
