import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class RegisterRequestDTO {
  @ApiProperty({ type: String, required: true })
  @IsNotEmpty({ message: 'An username is required' })
  @IsString({ message: 'Login must be a string' })
  readonly login: string;

  @ApiProperty({ type: String, required: true })
  @IsNotEmpty({ message: 'A password is required' })
  @MinLength(6, { message: 'Your password must be at least 6 characters' })
  readonly password: string;
}