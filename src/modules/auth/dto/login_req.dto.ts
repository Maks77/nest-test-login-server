import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginRequestDTO {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'A login is required' })
  @IsString({ message: 'Login must be a string' })
  readonly login: string;

  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'A password is required to login' })
  @IsString({ message: 'Password must be a string' })
  readonly password: string;
}