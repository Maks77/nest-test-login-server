import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RefreshRequestDTO {
  @ApiProperty({type: String, required: true})
  @IsNotEmpty({ message: 'The refresh token is required' })
  @IsString({ message: 'The refresh token must be a string' })
  readonly refresh_token: string;
}
