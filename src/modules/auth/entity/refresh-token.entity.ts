import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('refresh_tokens')
export class RefreshToken {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: string

  @Column()
  is_revoked: boolean

  @Column()
  expires: Date
}