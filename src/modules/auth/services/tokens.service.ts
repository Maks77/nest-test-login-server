import { Inject, Injectable, UnprocessableEntityException } from '@nestjs/common';
import { RefreshTokenRepository } from './refresh-token.repository';
import { UsersService } from '../../users/service/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../../users/entity/user.entity';
import { RefreshToken } from '../entity/refresh-token.entity';
import { SignOptions, TokenExpiredError } from 'jsonwebtoken';

export interface RefreshTokenPayload {
  jti: number;
  sub: string;
}

const BASE_OPTIONS: SignOptions = {
  issuer: 'http://localhost', // https://my-app.com
  audience:'http://localhost', // https://my-app.com
}

@Injectable()
export class TokensService {
  constructor(
    @Inject('REFRESH_REPOSITORY')
    private tokens: RefreshTokenRepository,
    private users: UsersService,
    private jwt: JwtService
  ) {
  }


  public async generateAccessToken(user: User): Promise<string> {
    const opts: SignOptions = {
      ...BASE_OPTIONS,
      subject: String(user.id),
    }

    return this.jwt.signAsync({}, opts)
  }

  public async generateRefreshToken(user: User, expiresIn: number): Promise<string> {
    const token = await this.tokens.createRefreshToken(user, expiresIn)

    const opts: SignOptions = {
      ...BASE_OPTIONS,
      expiresIn,
      subject: String(user.id),
      jwtid: String(token.id),
    }

    return this.jwt.signAsync({}, opts)
  }

  public async resolveRefreshToken(encoded: string): Promise<{user: User, refresh_token: RefreshToken}> {
    const payload = await this.decodeRefreshToken(encoded)
    const refresh_token = await this.getStoredTokenFromRefreshTokenPayload(payload)

    if (!refresh_token) {
      throw new UnprocessableEntityException('Refresh token not found')
    }

    if (refresh_token.is_revoked) {
      throw new UnprocessableEntityException('Refresh token revoked')
    }

    const user = await this.getUserFromRefreshTokenPayload(payload)

    if (!user) {
      throw new UnprocessableEntityException('Refresh token malformed')
    }

    return { user, refresh_token }
  }

  public async createAccessTokenFromRefreshToken(refresh: string): Promise<{token: string, user: User}> {
    const { user, refresh_token } = await this.resolveRefreshToken(refresh)

    // Remove used refresh_token from DB
    await this.tokens.deleteRefreshToken(refresh_token)

    const token = await this.generateAccessToken(user)

    return { user, token }
  }





  private async decodeRefreshToken(token: string): Promise<RefreshTokenPayload> {
    try {
      return this.jwt.verifyAsync(token)
    } catch (e) {
      if (e instanceof TokenExpiredError) {
        throw new UnprocessableEntityException('Refresh token expired')
      } else {
        throw new UnprocessableEntityException('Refresh token malformed')
      }
    }
  }

  private async getUserFromRefreshTokenPayload (payload: RefreshTokenPayload): Promise<User> {
    const subId = payload.sub

    if (!subId) {
      throw new UnprocessableEntityException('Refresh token malformed')
    }

    return this.users.findById(subId)
  }

  private async getStoredTokenFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<RefreshToken | null> {
    const tokenId = payload.jti

    if (!tokenId) {
      throw new UnprocessableEntityException('Refresh token malformed')
    }

    return this.tokens.findTokenById(tokenId)
  }
}