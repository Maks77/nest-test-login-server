import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../users/entity/user.entity';
import { DeleteResult, Repository } from 'typeorm';
import { RefreshToken } from '../entity/refresh-token.entity';

@Injectable()
export class RefreshTokenRepository {
  constructor(
    @InjectRepository(RefreshToken)
    private tokenRepository: Repository<RefreshToken>
  ) {
  }

  public async createRefreshToken(user: User, ttl: number): Promise<RefreshToken> {
    const token = new RefreshToken()
    const expiration = new Date()
    expiration.setTime(expiration.getTime() + ttl)

    token.user_id = user.id
    token.is_revoked = false
    token.expires = expiration

    return this.tokenRepository.save<RefreshToken>(token)
  }

  public async findTokenById(id: number): Promise<RefreshToken | null> {
    return this.tokenRepository.findOne({where: {id}})
  }

  public async deleteRefreshToken(token: RefreshToken): Promise<DeleteResult> {
    return this.tokenRepository.delete(token)
  }
}