import { Color } from '../models/color.entity';

export class UpdateNotesGroupDTO {
  id: string
  name?: string
  color?: Color
}