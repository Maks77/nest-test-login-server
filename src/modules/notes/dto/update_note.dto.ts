export class UpdateNoteDTO {
  id: string
  user_id: string
  title?: string
  text?: string
  group_id?: string
}