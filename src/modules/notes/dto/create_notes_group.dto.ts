import { Color } from '../models/color.entity';

export class CreateNotesGroupDTO {
  user_id: string
  name: string
  color?: Color
}