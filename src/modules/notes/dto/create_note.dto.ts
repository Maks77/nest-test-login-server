export class CreateNoteDTO {
  title?: string
  text?: string
  group_id?: string
}