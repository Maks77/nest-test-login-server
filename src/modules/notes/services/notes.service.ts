import { Injectable } from '@nestjs/common';
import { Note } from '../models/note.entity';
import { v4 as uuid } from 'uuid';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { from } from 'rxjs/internal/observable/from';
import { CreateNoteDTO } from '../dto/create_note.dto';
import { UpdateNoteDTO } from '../dto/update_note.dto';

@Injectable()
export class NotesService {
  private notesArr: Note[] = []

  constructor() {
    this.mockNotes()
  }

  private mockNotes(): void {
    for(let i = 0; i < 5; i++) {
      const note = new Note()
      note.id = uuid()
      note.user_id = i % 2 === 0 ? 8 : 22
      note.title = `Title ${i}`
      note.text = `Note text #${i}`

      this.notesArr.push(note)
    }
  }

  public async getAllNotes(): Promise<Note[]> {
    return this.notesArr
  }

  public async getAllNotesByUserId(userId: number): Promise<Note[]> {
    return this.notesArr.filter(note => note.user_id === userId)
  }

  public async createNote(userId: number, noteDTO: CreateNoteDTO): Promise<Note> {
    const newNote = new Note()
    newNote.id = uuid()
    newNote.user_id = userId
    newNote.title = noteDTO.title
    newNote.text = noteDTO.text
    newNote.group_id = noteDTO.group_id

    this.notesArr.push(newNote)
    return newNote
  }

  public async deleteNote(noteId: string): Promise<{deleted_id: string}> {
    const noteIndex = this.notesArr.map(note => note.id).indexOf(noteId)
    this.notesArr.splice(noteIndex, 1)
    return Promise.resolve({deleted_id: noteId})
  }

  public async updateNote(updateNoteDTO: UpdateNoteDTO): Promise<Note> {
    const existedNote = this.notesArr.find(note => note.id === updateNoteDTO.id)
    existedNote.text = updateNoteDTO.text ? updateNoteDTO.text : existedNote.text
    existedNote.title = updateNoteDTO.title ? updateNoteDTO.text : existedNote.title
    existedNote.group_id = updateNoteDTO.group_id ? updateNoteDTO.text : existedNote.group_id
    return existedNote
  }
}