import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Note } from '../models/note.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateNoteDTO } from '../dto/create_note.dto';
import { UpdateNoteDTO } from '../dto/update_note.dto';

@Injectable()
export class NotesRepository {
  constructor(
    @InjectRepository(Note)
    private repo: Repository<Note>
  ) {
  }

  public async getAllNotesByUserId(userId: number): Promise<Note[]> {
    return await this.repo.find({where: {user_id: userId}})
  }

  public async createNote(userId: number, noteDTO: CreateNoteDTO): Promise<Note> {
    const newNote = new Note()
    newNote.user_id = userId
    newNote.title = noteDTO.title
    newNote.text = noteDTO.text
    newNote.group_id = noteDTO.group_id
    return await this.repo.save(newNote)
  }

  public async deleteNote(noteId: string): Promise<DeleteResult> {
    return await this.repo.delete({id: noteId})
  }

  public async updateNote(updateNoteDTO: UpdateNoteDTO): Promise<UpdateResult> {
    const existedNote = await this.repo.findOne({where: {id: updateNoteDTO.id}})
    existedNote.text = updateNoteDTO.text ? updateNoteDTO.text : existedNote.text
    existedNote.title = updateNoteDTO.title ? updateNoteDTO.text : existedNote.title
    existedNote.group_id = updateNoteDTO.group_id ? updateNoteDTO.text : existedNote.group_id

    return this.repo.update({id: updateNoteDTO.id}, existedNote)
  }
}