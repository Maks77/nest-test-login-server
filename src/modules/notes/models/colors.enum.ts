export enum Colors {
  DEFAULT = '#ccc',
  RED = '#F84242',
  GREEN = '#4DCF59',
  BLUE = '#4B9DD1',
  ORANGE = '#F38B58'
}