import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('notes')
export class Note {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  user_id: number

  @Column({nullable: true})
  title?: string

  @Column({nullable: true})
  text?: string

  @Column({nullable: true})
  group_id?: string | null

  @CreateDateColumn({type: 'timestamp'})
  created_at: number

  @UpdateDateColumn({type: 'timestamp'})
  updated_at: number
}