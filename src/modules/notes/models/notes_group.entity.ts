import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('note_groups')
export class NotesGroup {

  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  user_id: string

  @Column()
  name: string

  @Column()
  color_id: string
}