import { Body, Controller, Delete, Get, Inject, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { JWTGuard } from '../../auth/guards/jwt.guard';
import { NotesService } from '../services/notes.service';
import { CreateNoteDTO } from '../dto/create_note.dto';
import { UpdateNoteDTO } from '../dto/update_note.dto';
import { NotesRepository } from '../services/notes.repository';

@ApiTags('Notes')
@Controller('note')
export class NotesController {
  constructor(
    // private notesService: NotesService
    private notesRepository: NotesRepository
  ) {}

  @Get()
  @UseGuards(JWTGuard)
  getAllNotes(@Req() request) {
    const userId = request.user.id
    return this.notesRepository.getAllNotesByUserId(userId)
  }

  @Post()
  @UseGuards(JWTGuard)
  createNote(@Req() request, @Body() body: CreateNoteDTO) {
    const userId = request.user.id
    return this.notesRepository.createNote(userId, body)
  }

  @Delete(':id')
  @UseGuards(JWTGuard)
  deleteNote(@Param('id') noteId: string) {
    return this.notesRepository.deleteNote(noteId)
  }

  @Put()
  @UseGuards(JWTGuard)
  updateNote(@Body() body: UpdateNoteDTO) {
    return this.notesRepository.updateNote(body)
  }
}
