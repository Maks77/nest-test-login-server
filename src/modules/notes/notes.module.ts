import { Module } from '@nestjs/common';
import { NotesController } from './controllers/notes.controller';
import { NotesService } from './services/notes.service';
import { NotesRepository } from './services/notes.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './models/note.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Note])
  ],
  controllers: [NotesController],
  providers: [
    NotesRepository,
    // {  provide: 'NOTES_REPOSITORY', useClass: NotesRepository }
  ]
})
export class NotesModule {}
